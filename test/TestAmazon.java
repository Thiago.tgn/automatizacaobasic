import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestAmazon {

    private WebDriver driver;


    @Before
    public void open (){
        System.setProperty("webdriver.gecko.driver",
                "/home/thiago/Curso/testamazon/src/test/resources/firefox/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br/");

    }

    @After
    public void exit () {

        driver.quit();
        }
    @Test
    public void  maisVendido (){
        driver.findElement(By.id("nv-hamburger-menu")).clear();
        driver.findElement(By.id("nav-hamburger-menu")).click();
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Assert.assertEquals("Mais vendidos", driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
    }

    @Test
    public void openTodos() throws InterruptedException {
        Thread.sleep(40000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(40000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Thread.sleep(40000);
        Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
    }
    @Test
    public void testPesquisaAmazon() {
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("RESULTADOS",driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        driver.quit();

    }
    @Test
    public void verificarAmazon() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("tgn2000000000000033392@gmail.com");
        driver.findElement(By.id("continue")).click();
        Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail", driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")).getText());
        driver.quit();
    }
    @Test
    public void openBooks () throws InterruptedException {
        Thread.sleep(4000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[6]")).click();
        Thread.sleep(4000);
        Assert.assertEquals("Loja de Livros", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div/div/div/div/div/div/div/div/h1")).getText());
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("selenium");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("RESULTADOS",driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
    }
    @Test
    public void testButtonAmazonPrime (){
        driver.findElement(By.id("nav-link-amazonprime")).click();
        Assert.assertEquals("Ainda mais motivos para amar o Amazon Prime:", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/div/div[2]/p")).getText());
    }
    @Test
    public void testButtonShoppingCar () throws InterruptedException {
        Thread.sleep(20000);
        driver.findElement(By.id("nav-cart")).click();
        Assert.assertEquals("Seu carrinho da Amazon está vazio",driver.findElement(By.xpath("/html/body/div[1]/div[4]/div/div[3]/div/div[2]/div[1]/div/div/div[2]/div[1]/h2")).getText());
    }
}

}
